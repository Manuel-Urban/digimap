from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', include('pages.urls')),
    path('accounts/', include('accounts.urls')),
    path('admin/', admin.site.urls),
    path('companies/', include('companies.urls')),
    path('diagrams/', include('diagrams.urls')),
    path('contact_form/', include('contact_form.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
