from django.contrib.messages import constants as messages
from django.utils.translation import ugettext_lazy as _
import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'sxyvs#1*dw$=@3w6tugg@8tz2s2kx=hm!31aui1t^nqt=h!=ri'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

# Application definition
INSTALLED_APPS = [
    'corsheaders',
    'companies.apps.CompaniesConfig',
    'users.apps.UsersConfig',
    'categories.apps.CategoriesConfig',
    'company_type.apps.CompanyTypeConfig',
    'diagrams.apps.DiagramsConfig',
    'contact_form.apps.ContactFormConfig',
    'leaflet',
    'pages.apps.PagesConfig',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'fontawesome',
    'django',
    'django.contrib.gis',
    'colorful'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'digimap.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.i18n',
            ],
        },
    },
]

WSGI_APPLICATION = 'digimap.wsgi.application'


# Database
DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.mysql',
        'NAME': 'digimapdb',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': 'localhost',
    }
}


# Password validation
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'digimap/static')
]

# Custom User Model
AUTH_USER_MODEL = 'users.CustomUser'

# Leaflet Map Configuration
LEAFLET_CONFIG = {
    'DEFAULT_CENTER': (54.184941, 9.788818),
    'DEFAULT_ZOOM': 7,
    'MIN_ZOOM': 3,
    'MAX_ZOOM': 10,
    'MINIMAP': True,
}

# Media folder settings
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/'

# Messages
MESSAGE_TAGS = {
    messages.ERROR: 'danger',
}

# Email Config
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'techmap.hsfl@gmail.com'
EMAIL_HOST_PASSWORD = 'hsfldigimap'
EMAIL_USE_TLS = True

# CORS WHITELIST
CORS_ORIGIN_WHITELIST = (
    'localhost:8000/pages/searchAJAX'
)

# Translation
MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
)

# Provide a lists of languages which your site supports.
LANGUAGES = (
    ('en', _('English')),
    ('de', _('German')),
)

# Set the default language for your site.
LANGUAGE_CODE = 'de'
USE_L10N = True
USE_I18N = True

# Tell Django where the project's translation files should be.
LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

LANGUAGE_CHOICES = (
    ('en', 'English'),
    ('de', 'German'),
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.i18n',  
)
