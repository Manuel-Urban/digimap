# Generated by Django 2.2.4 on 2020-03-24 09:05

import datetime
from django.conf import settings
import django.core.files.storage
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('categories', '0001_initial'),
        ('company_type', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('company_name', models.CharField(max_length=200)),
                ('photo', models.ImageField(blank=True, storage=django.core.files.storage.FileSystemStorage(), upload_to='')),
                ('email', models.CharField(max_length=100)),
                ('phone', models.CharField(max_length=100)),
                ('address', models.CharField(max_length=100)),
                ('zipcode', models.IntegerField()),
                ('city', models.CharField(max_length=100)),
                ('description', models.TextField(blank=True, max_length=1000)),
                ('creation_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('is_published', models.BooleanField(default=False)),
                ('lon', models.CharField(blank=True, max_length=100)),
                ('lat', models.CharField(blank=True, max_length=100)),
                ('category_1', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='Category_1', to='categories.Category')),
                ('category_2', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='Category_2', to='categories.Category')),
                ('category_3', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='Category_3', to='categories.Category')),
                ('company_type', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='CompanyType', to='company_type.CompanyType')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Unternehmen',
                'verbose_name_plural': 'Alle Unternehmen',
            },
        ),
        migrations.CreateModel(
            name='ContactPerson',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('photo', models.ImageField(blank=True, storage=django.core.files.storage.FileSystemStorage(), upload_to='')),
                ('email', models.EmailField(max_length=200)),
                ('phone', models.CharField(max_length=20)),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='companies.Company')),
            ],
            options={
                'verbose_name': 'Kontaktperson',
                'verbose_name_plural': 'Alle Kontaktpersonen',
            },
        ),
    ]
