from django.test import TestCase
from .models import Company
from users.models import CustomUser


class CompanyTestCase(TestCase):
    def setUp(self):
        user = CustomUser(email="admin@gmail.com",
                          password="admin")
        user.save()
        company = Company(company_name="Test-Unternehmen",
                          email="test@test.de",
                          phone="01722776659",
                          address="Auf dem Campus 6",
                          lat=54.777050,
                          lon=9.455960,
                          zipcode=24943,
                          city="Flensburg",
                          user=user)
        company.save()

    def test_company_exist(self):
        test = Company.objects.get(company_name="Test-Unternehmen")
        user = CustomUser.objects.get(email="admin@gmail.com")
        self.assertEqual(test.email, "test@test.de")
        self.assertEqual(test.phone, "01722776659")
        self.assertEqual(test.address, "Auf dem Campus 6")
        self.assertEqual(test.zipcode, 24943)
        self.assertEqual(test.city, "Flensburg")
        self.assertEqual(test.user, user)
