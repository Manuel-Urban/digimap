from django.urls import path
from . import views

urlpatterns = [
    path('create_company', views.create_company, name='create_company'),
    path('delete_company', views.delete_company, name='delete_company'),
    path('update_company', views.update_company, name='update_company'),
    path('<int:company_ID>', views.detail_company, name='detail_company'),
    path('contact_person', views.contact_person, name='contact_person'),
    path('update_contact_person', views.update_contact_person,
         name='update_contact_person')
]
