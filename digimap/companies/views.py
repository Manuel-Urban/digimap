from django.shortcuts import redirect, render
from django.contrib import messages
from .models import Company, ContactPerson
from categories.models import Category
from company_type.models import CompanyType
from users.models import CustomUser
from geopy.geocoders import Nominatim
from django.core import serializers
from django.core.files.storage import FileSystemStorage

# creates a new company 
def create_company(request):
    if request.method == 'POST' and 'photo' in request.FILES:
        company_name = request.POST['company_name']
        photo = request.FILES['photo']
        email = request.POST['email']
        phone = request.POST['phone']
        address = request.POST['address']
        zipcode = request.POST['zipcode']
        city = request.POST['city']
        user_id = request.POST['user_id']
        description = request.POST['description']
        category_1 = request.POST['category_1']
        category_2 = request.POST['category_3']
        category_3 = request.POST['category_3']

        # convert company name to ID
        company_type = CompanyType.objects.filter(
            company_type_name=request.POST['company_type']).values('id')[0]['id']
        company_type_save = CompanyType.objects.get(pk=company_type)

        user = CustomUser.objects.get(pk=user_id)

        # check which categories are chosen and save them with their IDs
        if category_1 == 'Keine Kategorie':
            category_1_save = None
        else:
            category_1_id = Category.objects.filter(
                category_name=category_1).values('id')[0]['id']
            category_1_save = Category.objects.get(pk=category_1_id)

        if category_2 == 'Keine Kategorie':
            category_2_save = None
        else:
            category_2_id = Category.objects.filter(
                category_name=request.POST['category_2']).values('id')[0]['id']
            category_2_save = Category.objects.get(pk=category_2_id)

        if category_3 == 'Keine Kategorie':
            category_3_save = None
        else:
            category_3_id = Category.objects.filter(
                category_name=request.POST['category_3']).values('id')[0]['id']
            category_3_save = Category.objects.get(pk=category_3_id)

        # save company logo
        fs = FileSystemStorage()
        uploaded_photo = fs.save(photo.name, photo)

        company = Company(
            company_name=company_name,
            photo=fs.url(uploaded_photo),
            email=email,
            phone=phone,
            address=address,
            zipcode=zipcode,
            city=city,
            category_1=category_1_save,
            category_2=category_2_save,
            category_3=category_3_save,
            company_type=company_type_save,
            description=description,
            user=user
        )
        company.save()

        messages.success(
            request, 'Ihr Unternehmen wurde erfolgreich angelegt!')
        return redirect('/accounts/dashboard')

# delete company with ID 
def delete_company(request):
    company = Company.objects.get(pk=request.POST['company_delete'])
    company.delete()
    messages.success(request, 'Das Unternehmen wurde gelöscht!')
    return redirect('/accounts/dashboard')

# update company properties
def update_company(request):
    if request.method == 'POST' and 'photo' in request.FILES:
        company = Company.objects.get(pk=request.POST['company_update'])
        company.company_name = request.POST['company_name']
        photo = request.FILES['photo']
        company.email = request.POST['email']
        company.phone = request.POST['phone']
        company.address = request.POST['address']
        company.zipcode = request.POST['zipcode']
        company.city = request.POST['city']
        company.user_id = request.POST['user_id']
        company.description = request.POST['description']
        category_1 = request.POST['category_1']
        category_2 = request.POST['category_3']
        category_3 = request.POST['category_3']
        company_type = CompanyType.objects.filter(
            company_type_name=request.POST['company_type']).values('id')[0]['id']
        company.company_type = CompanyType.objects.get(pk=company_type)

        fs = FileSystemStorage()
        uploaded_photo = fs.save(photo.name, photo)

        company.photo = fs.url(uploaded_photo)

        if category_1 == 'Keine Kategorie':
            company.category_1 = None
        else:
            category_1 = Category.objects.filter(
                category_name=category_1).values('id')[0]['id']
            company.category_1 = Category.objects.get(pk=category_1)

        if category_2 == 'Keine Kategorie':
            company.category_2 = None
        else:
            category_2 = Category.objects.filter(
                category_name=request.POST['category_2']).values('id')[0]['id']
            company.category_2 = Category.objects.get(pk=category_2)

        if category_3 == 'Keine Kategorie':
            company.category_3 = None
        else:
            category_3 = Category.objects.filter(
                category_name=request.POST['category_3']).values('id')[0]['id']
            company.category_3 = Category.objects.get(pk=category_3)

        company.save()

        return redirect('/accounts/dashboard')

# gets all detail properties about a company and shows them on the company page
def detail_company(request, company_ID):
    companies = Company.objects.get(pk=company_ID)

    # search for the contact person which belongs to the company
    try:
        contact_person = ContactPerson.objects.get(company=company_ID)
    except ContactPerson.DoesNotExist:
        contact_person = None

    # get all categories which belong to the company
    category_1 = companies.category_1
    category_2 = companies.category_2
    category_3 = companies.category_3
    all_categories = str(category_1) + str(category_2) + str(category_3)

    context = {
        'companies': companies,
        'contact_person': contact_person,
        'all_categories': all_categories,
        'category_1': category_1,
        'category_2': category_2,
        'category_3': category_3
    }
    return render(request, 'pages/company.html', context)

# create a contact person
# contactz person has link to company
def contact_person(request):
    if request.method == 'POST' and 'contact_person_photo' in request.FILES:
        name = request.POST['contact_person_name']
        photo = request.FILES['contact_person_photo']
        email = request.POST['contact_person_email']
        phone = request.POST['contact_person_phone']
        company_id = request.POST['contact_person_company']

        company = Company.objects.get(pk=company_id)

        fs = FileSystemStorage()
        uploaded_photo = fs.save(photo.name, photo)

        contact_person = ContactPerson(
            name=name,
            photo=fs.url(uploaded_photo),
            email=email,
            phone=phone,
            company=company
        )

        contact_person.save()

        messages.success(
            request, 'Ihre Kontaktperson wurde erfolgreich angelegt!')
        return redirect('/accounts/dashboard')

# update contact person 
def update_contact_person(request):
    if request.method == 'POST' and 'contact_person_photo' in request.FILES:
        contact_person = ContactPerson.objects.get(
            pk=request.POST['contact_person_id'])
        contact_person.name = request.POST['contact_person_name']
        photo = request.FILES['contact_person_photo']
        fs = FileSystemStorage()
        uploaded_photo = fs.save(photo.name, photo)
        contact_person.phone = uploaded_photo
        contact_person.email = request.POST['contact_person_email']
        contact_person.phone = request.POST['contact_person_phone']
        company_id = request.POST['contact_person_company']
        contact_person.company = Company.objects.get(pk=company_id)

        contact_person.save()

        messages.success(
            request, 'Ihre Kontaktperson wurde erfolgreich bearbeitet!')
        return redirect('/accounts/dashboard')
