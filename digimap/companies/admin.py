from django.contrib import admin
from .models import Company, ContactPerson


class CompanyAdmin(admin.ModelAdmin):
    list_display = ('id', 'company_name', 'email', 'user',
                    'creation_date', 'is_published')
    list_display_links = ('id', 'company_name')
    search_fields = ('company_name', 'email')
    list_per_page = 25
    exclude = ('lat', 'lon', 'user_id')


class ContactPersonAdmin(admin.ModelAdmin):
    list_display = ('name', 'photo', 'email', 'phone', 'company')
    list_display_links = ('name',)
    list_per_page = 25


admin.site.register(Company, CompanyAdmin)
admin.site.register(ContactPerson, ContactPersonAdmin)
