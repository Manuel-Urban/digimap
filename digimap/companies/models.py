from django.db import models
from datetime import datetime
from geopy.geocoders import Nominatim
from geopy.exc import GeocoderTimedOut
from django.core.files.storage import FileSystemStorage

fs = FileSystemStorage()

# sets all important company properties in db
class Company(models.Model):
    company_name = models.CharField(max_length=200)
    photo = models.ImageField(storage=fs, blank=True)
    email = models.CharField(max_length=100)
    phone = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    zipcode = models.IntegerField()
    city = models.CharField(max_length=100)
    company_type = models.CharField(max_length=100)
    category_1 = models.ForeignKey(
        'categories.Category', on_delete=models.PROTECT, blank=True, null=True, related_name='Category_1')
    category_2 = models.ForeignKey(
        'categories.Category', on_delete=models.PROTECT, blank=True, null=True, related_name='Category_2')
    category_3 = models.ForeignKey(
        'categories.Category', on_delete=models.PROTECT, blank=True, null=True, related_name='Category_3')
    company_type = models.ForeignKey(
        'company_type.CompanyType', on_delete=models.PROTECT, null=True, related_name='CompanyType')
    description = models.TextField(max_length=1000, blank=True)
    creation_date = models.DateTimeField(default=datetime.now, blank=True)
    is_published = models.BooleanField(default=False)
    user = models.ForeignKey('users.CustomUser', on_delete=models.PROTECT)
    lon = models.CharField(max_length=100, blank=True)
    lat = models.CharField(max_length=100, blank=True)

    # gets the address and convert it to lon and lat to show it on the map
    # It is a function in the model to update from the admin side as well as from the website
    def setCoordinates(self):
        fullAdress = self.address + "," + " " + \
            str(self.zipcode) + " " + self.city
        geolocator = Nominatim(user_agent="map")
        location = geolocator.geocode(fullAdress)
        self.lon = location.longitude
        self.lat = location.latitude

    def save(self, **kwargs):
        try:
            self.setCoordinates()
            super().save(**kwargs)
        except GeocoderTimedOut as e:
            print('Coordination error')

    def __str__(self):
        return self.company_name

    class Meta:
        verbose_name = 'Unternehmen'
        verbose_name_plural = 'Alle Unternehmen'

# sets all important contacts person properties in db
# the contact person has the link to the company
class ContactPerson(models.Model):
    name = models.CharField(max_length=200)
    photo = models.ImageField(storage=fs, blank=True)
    email = models.EmailField(max_length=200)
    phone = models.CharField(max_length=20)
    company = models.ForeignKey('Company', on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Kontaktperson'
        verbose_name_plural = 'Alle Kontaktpersonen'
