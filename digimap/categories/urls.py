from django.urls import path
from . import views

urlpatterns = [
    path('', views.show_categories, name='categories'),
]