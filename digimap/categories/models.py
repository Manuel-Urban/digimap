from django.db import models
from colorful.fields import RGBColorField


class Category(models.Model):
    category_name = models.CharField(max_length=200)
    category_icon = models.CharField(max_length=200, blank=True)
    category_color = RGBColorField()

    def __str__(self):
        return self.category_name

    class Meta:
        verbose_name = 'Kategorie'
        verbose_name_plural = 'Alle Kategorien'