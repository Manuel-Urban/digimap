from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, {'category': None,
                           'company_type': None}, name='index'),
    path('pages/<str:category>/<str:company_type>/', views.index, name='index'),
    path('pages/<str:company_type>', views.index, name='index'),
    path('pages/<str:category>', views.index, name='index'),
    path('/impressum', views.impressum, name='impressum'),
    path('/gdpr', views.gdpr, name='gdpr'),
    path('/search', views.search, name='search'),
]
