from django.shortcuts import render
from companies.models import Company
from categories.models import Category
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db.models import Q
from django.core import serializers
from django.http import HttpResponse
from company_type.models import CompanyType

# fill index page with infos


def index(request, category, company_type):
    # check if category and type are both correct
    map_fitting_companies = None

    companies = Company.objects.order_by(
        '-creation_date').filter(is_published=True)

    #sets a paginator to make the list of all found companies more readable
    paginator = Paginator(companies, 4)
    page = request.GET.get('page')
    paged_companies = paginator.get_page(page)

    categories = Category.objects.all()

    if(company_type == None):
        company_type = 'Alle'

    # if company_type is all, than there is no filter
    if (company_type == 'Alle'):
        current_category = Category.objects.filter(
            category_name=category).first()
        map_fitting_companies = Company.objects.filter(Q(category_1=current_category) | Q(
            category_2=current_category) | Q(category_3=current_category))

    # filter company type
    if(company_type != 'Alle'):
        type_name = CompanyType.objects.filter(
            company_type_name=company_type).values('id')[0]['id']
        current_category = Category.objects.filter(
            category_name=category).first()
        map_fitting_companies = Company.objects.filter((Q(category_1=current_category) | Q(
            category_2=current_category) | Q(category_3=current_category)) & Q(company_type=type_name))

    # current chosen filters
    current_chosen_category = category
    current_chosen_company_type = company_type

    # all company types for dropdown
    all_company_types = CompanyType.objects.all()

    context = {
        'companies': paged_companies,
        'categories': categories,
        'map_fitting_companies': map_fitting_companies,
        'current_chosen_category': current_chosen_category,
        'current_chosen_company_type': current_chosen_company_type,
        'all_company_types': all_company_types
    }
    return render(request, 'pages/index.html', context)

# defines the search for companies


def search(request):
    companies = Company.objects.all()
    # Keywords
    if 'keywords' in request.GET:
        keywords = request.GET['keywords']
        if keywords:
            companies = Company.objects.order_by(
                '-creation_date').filter(is_published=True, company_name__icontains=keywords)

    # sets a paginator to make the list of all found companies more readable
    paginator = Paginator(companies, 4)
    page = request.GET.get('page')
    paged_companies = paginator.get_page(page)
    context = {
        'companies': paged_companies
    }

    return render(request, 'pages/search.html', context)

# navigates to impressum
def impressum(request):
    return render(request, 'pages/impressum.html')

# navigate to gdpr
def gdpr(request):
    return render(request, 'pages/gdpr.html')
