from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager


class CustomUserManager(BaseUserManager):
    def create_user(self, email, password=None):
        if not email:
            raise ValueError("Users must have an email address")
        if not password:
            raise ValueError("Users must have a password")
        user = self.model(
            email=self.normalize_email(email)
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password=None):
        user = self.create_user(
            email,
            password=password,
        )
        user.is_staff = True
        user.is_admin = True
        user.save(using=self._db)
        return user


class CustomUser(AbstractBaseUser):
    email = models.EmailField(
        verbose_name='E-Mail-Addresse', max_length=255, unique=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    confirm = models.BooleanField(
        verbose_name='Bestätigter Nutzer', default=False)
    is_staff = models.BooleanField(default=False)
    is_admin = models.BooleanField(verbose_name='Admin', default=False)
    is_active = models.BooleanField(default=True)

    USERNAME_FIELD = 'email'
    # USERNAME_FIELD and password are required by default
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.email

    def has_module_perms(self, app_label):
        return True

    def has_perm(self, perm, obj=None):
        return True

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Alle User'
