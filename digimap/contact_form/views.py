from django.shortcuts import render, redirect
from django.contrib import messages
from django.core.mail import send_mail
from .models import Contact

# navigate to contact form page


def contact_form(request):
    return render(request, 'contact/contact.html')

# create contact form for users
# admin gets an notification email


def contact(request):
    if request.method == 'POST':
        title = request.POST['title']
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        email = request.POST['email']
        phone = request.POST['phone']
        description = request.POST['description']

        contact = Contact(
            title=title,
            first_name=first_name,
            last_name=last_name,
            email=email,
            phone=phone,
            description=description,
        )
        contact.save()

        # email content
        send_mail(
            'Kontaktanfrage',
            'Sie haben eine neue Kontaktanfrage. Loggen Sie sich für weitere Infos auf der Admin-Seite ein.',
            'techmap.hsfl@gmail.com',
            ['benjamin.schulz@hs-flensburg.de'],
            fail_silently=False,
        )

        messages.success(
            request, 'Ihre Anfrage wurde erfolgreich abgeschickt!')

        return redirect('index')
