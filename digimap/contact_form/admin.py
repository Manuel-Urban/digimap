from django.contrib import admin
from .models import Contact

class ContactFormAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'first_name', 'last_name', 'email', 'description')
    list_display_links = ('id', 'title', 'email')
    search_fields = ('title', 'email')
    list_per_page = 25

admin.site.register(Contact, ContactFormAdmin)