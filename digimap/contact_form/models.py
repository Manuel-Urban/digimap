from django.db import models


class Contact(models.Model):
    title = models.CharField(max_length=200)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    email = models.CharField(max_length=200)
    phone = models.CharField(max_length=200)
    description = models.TextField(max_length=300)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Kontaktanfrage'
        verbose_name_plural = 'Alle Kontaktanfragen'
