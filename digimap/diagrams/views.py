from django.shortcuts import render
from companies.models import Company
from categories.models import Category
from company_type.models import CompanyType

def diagrams(request):
    # get all categories
    labels_category_names = Category.objects.all()
    label_name = []
    label_id = []
    label_count = [] 
    
    # make label array
    for name in labels_category_names:
        label_name.append(str(name))

    # convert category name to id, because the company saves the id
    for i in range(len(label_name)):
        category_temp = Category.objects.filter(category_name=label_name[i]).values('id')[0]['id']
        label_id.append(category_temp)
        # fill label_count with 0
        label_count.append(0)

    # fill count array with number of found categories
    for i in range(len(label_id)):
        if Company.objects.filter(category_1=label_id[i]):
            all_companies_temp = Company.objects.filter(category_1=label_id[i])
            label_count[i] = label_count[i] + len(all_companies_temp)

        if Company.objects.filter(category_2=label_id[i]):
            label_count[i] += 1

        if Company.objects.filter(category_3=label_id[i]):
            label_count[i] += 1
    
    context = {
        'label_name': label_name,
        'label_count': label_count,
    }

    return render(request, 'diagrams/showDiagrams.html', context)