from django.shortcuts import render, redirect
from django.contrib import messages, auth
from users.models import CustomUser as User
from categories.models import Category
from company_type.models import CompanyType
from companies.models import Company, ContactPerson

# register a new person
def register(request):
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']
        password2 = request.POST['password2']

        if password == password2:
            if User.objects.filter(email=email).exists():
                messages.error(request, 'Email is being used')
                return redirect('register')
            else:
                # E-Mail does not exist yet, so User can be created
                user = User.objects.create_user(password=password, email=email)
                user.save()
                messages.success(
                    request, 'You are now registered and can log in')
                
                # Login after register, alternative to just saving and redirecting to login screen:
                return redirect('login')
        else:
            messages.error(request, 'Passwords do not match')
            return redirect('register')
    else:
        return render(request, 'accounts/register.html')

# login via email and password
def login(request):
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']

        user = auth.authenticate(email=email, password=password)

        if user is not None:
            auth.login(request, user)
            messages.success(request, 'You are now logged in')
            return redirect('dashboard')
        else:
            messages.error(request, 'Invalid credentials')
            return redirect('login')
    else:
        return render(request, 'accounts/login.html')

def logout(request):
    if request.method == 'POST':
        auth.logout(request)
        messages.success(request, 'You are now logged out')
    return redirect('index')

# dashbord to manage companies and their contact persons
def dashboard(request):
    if request.user.is_authenticated:
        companies = Company.objects.filter(user_id=request.user.id)
        categories = Category.objects.all()
        company_type = CompanyType.objects.all()
        contact_person = ContactPerson.objects.all()
        no_contact_person = ContactPerson.objects.all()

        context = {
            'categories': categories,
            'company_type': company_type,
            'companies': companies,
            'contact_person': contact_person,
            'no_contact_person': no_contact_person
        }

        return render(request, 'accounts/dashboard.html', context)
    else:
        return redirect('login')
