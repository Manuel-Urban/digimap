from django.apps import AppConfig


class CompanyTypeConfig(AppConfig):
    name = 'company_type'
    verbose_name = 'Unternehmenstypen'
