from django.db import models


class CompanyType(models.Model):
    company_type_name = models.CharField(max_length=200)

    def __str__(self):
        return self.company_type_name

    class Meta:
        verbose_name = 'Unternehmenstyp'
        verbose_name_plural = 'Alle Unternehmenstypen'
