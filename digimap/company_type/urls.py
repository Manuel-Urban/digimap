from django.urls import path
from . import views

urlpatterns = [
    path('', views.show_company_type, name='company_type'),
]
