from django.contrib import admin
from .models import CompanyType


class CompanyTypeAdmin(admin.ModelAdmin):
    list_display = ('company_type_name',)
    list_display_links = ('company_type_name',)
    search_fields = ('company_type_name',)
    list_per_page = 25


admin.site.register(CompanyType, CompanyTypeAdmin)
