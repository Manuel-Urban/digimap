# Generated by Django 2.2.4 on 2020-03-24 09:05

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CompanyType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('company_type_name', models.CharField(max_length=200)),
            ],
            options={
                'verbose_name': 'Unternehmenstyp',
                'verbose_name_plural': 'Alle Unternehmenstypen',
            },
        ),
    ]
