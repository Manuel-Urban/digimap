-- MySQL dump 10.13  Distrib 8.0.18, for macos10.14 (x86_64)
--
-- Host: localhost    Database: digimapdb
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `companies_contactperson`
--

DROP TABLE IF EXISTS `companies_contactperson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `companies_contactperson` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_general_ci NOT NULL,
  `photo` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(200) COLLATE utf8mb4_general_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_general_ci NOT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `companies_contactper_company_id_e1299f95_fk_companies` (`company_id`),
  CONSTRAINT `companies_contactper_company_id_e1299f95_fk_companies` FOREIGN KEY (`company_id`) REFERENCES `companies_company` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies_contactperson`
--

LOCK TABLES `companies_contactperson` WRITE;
/*!40000 ALTER TABLE `companies_contactperson` DISABLE KEYS */;
INSERT INTO `companies_contactperson` VALUES (1,'Hans Müller','photos/contact_person/viking-4898879_1920.jpg','hans.mueller@mac-its.com','324567887665',1),(2,'Max Musterman','photos/contact_person/viking-4898879_1920_gVpFQnw.jpg','max.musterman@greylogix.com','1234456789',2),(3,'Marie Mars','photos/contact_person/viking-4898879_1920_qIU5yrQ.jpg','marie.mars@kabel.com','546378765436',3),(4,'Sophie Paulsen','photos/contact_person/viking-4898879_1920_7b5ggG0.jpg','sophie.paulsen@fb.com','657489098887',4),(5,'Martin Hansen','photos/contact_person/viking-4898879_1920_4u6vjtE.jpg','martin.hansen@nordbase.com','546322445566',5),(6,'Norbert Salzer','photos/contact_person/viking-4898879_1920_tULy4Uh.jpg','norbert.salzer@prisma.com','546378987654',6),(7,'Anika Braasch','photos/contact_person/viking-4898879_1920_2zzip9S.jpg','anika.braasch@consist.com','546378988876',7),(8,'Leon Meier','photos/contact_person/viking-4898879_1920_Xd34aa7.jpg','leon.meier@ihk.com','543211345567',8),(9,'Matthias Vossen','photos/contact_person/viking-4898879_1920_lt1vizG.jpg','matthias.vossen@web.com','764532415263',9),(10,'Lena Schlüter','photos/contact_person/viking-4898879_1920_vc08y1h.jpg','lena.schlueter@it.com','987654323456',10),(11,'Ralf Münster','photos/contact_person/viking-4898879_1920_bGaYxJc.jpg','ralf.muenster@sgb.com','456778987600',11),(12,'Finn Petersen','photos/contact_person/viking-4898879_1920_JmXxtQI.jpg','finn.petersen@maris.com','123456789098',12),(13,'Marvin Klaus','photos/contact_person/viking-4898879_1920_vf2LoxN.jpg','marvin.klaus@bosch.com','435262726534',13),(14,'Lene Hansen','photos/contact_person/viking-4898879_1920_SuI1O1Q.jpg','lene.hansen@nordland.com','65435261789',14),(15,'Eva Kasiske','photos/contact_person/viking-4898879_1920_l39SADC.jpg','eva.kasiske@datablock.com','123456655543',15),(16,'Paul Paulsen','photos/contact_person/viking-4898879_1920_oPi9p4F.jpg','paul.paulsen@secret.com','666777889634',16),(17,'Sina Stark','photos/contact_person/viking-4898879_1920_nMrBCMg.jpg','sina.stark@siemens.com','453678776654',17),(18,'Fred Carstensen','photos/contact_person/viking-4898879_1920_flZND7y.jpg','fred.carstensen@axa.com','5463778898756',18),(19,'Nele Nielsen','photos/contact_person/viking-4898879_1920_r6ZfOXN.jpg','nele.nielsen@c319.com','4536243567',19);
/*!40000 ALTER TABLE `companies_contactperson` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-15 23:29:36
