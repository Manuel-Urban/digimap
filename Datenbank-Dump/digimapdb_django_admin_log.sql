-- MySQL dump 10.13  Distrib 8.0.18, for macos10.14 (x86_64)
--
-- Host: localhost    Database: digimapdb
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext COLLATE utf8mb4_general_ci,
  `object_repr` varchar(200) COLLATE utf8mb4_general_ci NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext COLLATE utf8mb4_general_ci NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_users_customuser_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_users_customuser_id` FOREIGN KEY (`user_id`) REFERENCES `users_customuser` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2020-03-13 16:17:09.693262','1','AR/VR',1,'[{\"added\": {}}]',5,1),(2,'2020-03-13 16:17:23.675363','2','Beratung',1,'[{\"added\": {}}]',5,1),(3,'2020-03-13 16:17:38.710686','3','Cloud',1,'[{\"added\": {}}]',5,1),(4,'2020-03-13 16:17:58.587149','4','Design',1,'[{\"added\": {}}]',5,1),(5,'2020-03-13 16:18:23.955696','5','Filmproduktion',1,'[{\"added\": {}}]',5,1),(6,'2020-03-13 16:18:41.267282','6','Forschung',1,'[{\"added\": {}}]',5,1),(7,'2020-03-13 16:19:22.065344','7','Hardware',1,'[{\"added\": {}}]',5,1),(8,'2020-03-13 16:19:43.248697','8','IT-Systemhaus',1,'[{\"added\": {}}]',5,1),(9,'2020-03-13 16:20:39.878048','9','KI',1,'[{\"added\": {}}]',5,1),(10,'2020-03-13 16:21:06.712597','10','Netzwerktechnik',1,'[{\"added\": {}}]',5,1),(11,'2020-03-13 16:21:35.349975','11','Web-Entwicklung',1,'[{\"added\": {}}]',5,1),(12,'2020-03-13 16:22:08.925396','12','Software-Entwicklung',1,'[{\"added\": {}}]',5,1),(13,'2020-03-13 16:25:43.288702','1','AR VR',2,'[{\"changed\": {\"fields\": [\"category_name\"]}}]',5,1),(14,'2020-03-13 16:27:09.227974','1','StartUp',1,'[{\"added\": {}}]',6,1),(15,'2020-03-13 16:27:14.169332','2','Öffentlicher Dienst',1,'[{\"added\": {}}]',6,1),(16,'2020-03-13 16:27:26.185143','3','Verein',1,'[{\"added\": {}}]',6,1),(17,'2020-03-13 16:33:03.347298','4','Personenunternehmen',1,'[{\"added\": {}}]',6,1),(18,'2020-03-13 16:33:19.394436','5','Kapitalgesellschaft',1,'[{\"added\": {}}]',6,1),(19,'2020-03-13 16:33:33.669426','6','Eingetragene Genossenschaft',1,'[{\"added\": {}}]',6,1),(20,'2020-03-13 17:04:51.369234','1','MAC IT-Solution',1,'[{\"added\": {}}]',2,1),(21,'2020-03-13 17:05:07.301000','1','MAC IT-Solution',2,'[{\"changed\": {\"fields\": [\"creation_date\"]}}]',2,1),(22,'2020-03-13 17:12:12.103876','2','Bilfinger GreyLogix GmbH',1,'[{\"added\": {}}]',2,1),(23,'2020-03-13 17:14:08.783675','3','Kabel Technik Kiel GmbH',1,'[{\"added\": {}}]',2,1),(24,'2020-03-13 17:17:15.739969','4','FB datentechnik',1,'[{\"added\": {}}]',2,1),(25,'2020-03-13 17:19:04.803377','5','nordbase IT',1,'[{\"added\": {}}]',2,1),(26,'2020-03-13 17:20:44.492447','6','Prisma Computertechnik',1,'[{\"added\": {}}]',2,1),(27,'2020-03-13 17:25:24.190874','7','Consist Software Solutions',1,'[{\"added\": {}}]',2,1),(28,'2020-03-13 17:28:09.660437','8','IHK Flensburg',1,'[{\"added\": {}}]',2,1),(29,'2020-03-13 17:33:15.210000','9','Web - Matthias Vossen',1,'[{\"added\": {}}]',2,1),(30,'2020-03-13 17:33:53.777069','9','Web - Matthias Vossen',2,'[{\"changed\": {\"fields\": [\"photo\"]}}]',2,1),(31,'2020-03-13 17:36:26.918630','9','Web - Matthias Vossen',2,'[{\"changed\": {\"fields\": [\"photo\"]}}]',2,1),(32,'2020-03-13 17:37:42.465204','1','MAC IT-Solution',2,'[{\"changed\": {\"fields\": [\"photo\"]}}]',2,1),(33,'2020-03-13 19:35:33.435130','10','IT - Walther Kassen',1,'[{\"added\": {}}]',2,1),(34,'2020-03-13 19:37:29.965334','11','SGB IT',1,'[{\"added\": {}}]',2,1),(35,'2020-03-13 19:37:42.059429','10','IT - Walther Kassen',2,'[{\"changed\": {\"fields\": [\"photo\"]}}]',2,1),(36,'2020-03-13 19:40:24.005628','12','Maris Computer',1,'[{\"added\": {}}]',2,1),(37,'2020-03-13 19:53:54.088710','13','Bosch Data',1,'[{\"added\": {}}]',2,1),(38,'2020-03-13 19:56:28.101807','14','Nordland IT Medien',1,'[{\"added\": {}}]',2,1),(39,'2020-03-13 19:58:55.621651','15','DATABLOCK EDV-Systeme',1,'[{\"added\": {}}]',2,1),(40,'2020-03-13 20:01:14.227087','16','Secret Software',1,'[{\"added\": {}}]',2,1),(41,'2020-03-13 20:03:51.432134','17','Siemens AG',1,'[{\"added\": {}}]',2,1),(42,'2020-03-13 20:05:39.336875','18','AXA Stephan Lamp',1,'[{\"added\": {}}]',2,1),(43,'2020-03-13 20:07:23.199695','19','C319 IT Services',1,'[{\"added\": {}}]',2,1),(44,'2020-03-13 20:07:44.122981','1','MAC IT-Solution',2,'[{\"changed\": {\"fields\": [\"creation_date\"]}}]',2,1),(45,'2020-03-13 20:08:01.170396','4','FB datentechnik',2,'[{\"changed\": {\"fields\": [\"creation_date\"]}}]',2,1),(46,'2020-03-13 20:08:22.339823','5','nordbase IT',2,'[{\"changed\": {\"fields\": [\"creation_date\"]}}]',2,1),(47,'2020-03-13 20:08:38.070992','6','Prisma Computertechnik',2,'[{\"changed\": {\"fields\": [\"creation_date\"]}}]',2,1),(48,'2020-03-13 20:08:47.976846','7','Consist Software Solutions',2,'[{\"changed\": {\"fields\": [\"creation_date\"]}}]',2,1),(49,'2020-03-13 20:08:59.561881','8','IHK Flensburg',2,'[{\"changed\": {\"fields\": [\"creation_date\"]}}]',2,1),(50,'2020-03-13 20:09:11.141145','9','Web - Matthias Vossen',2,'[{\"changed\": {\"fields\": [\"creation_date\"]}}]',2,1),(51,'2020-03-13 20:09:31.155758','10','IT - Walther Kassen',2,'[{\"changed\": {\"fields\": [\"creation_date\"]}}]',2,1),(52,'2020-03-13 20:09:45.113111','11','SGB IT',2,'[{\"changed\": {\"fields\": [\"creation_date\"]}}]',2,1),(53,'2020-03-13 20:09:55.597221','12','Maris Computer',2,'[{\"changed\": {\"fields\": [\"creation_date\"]}}]',2,1),(54,'2020-03-13 20:10:08.749504','13','Bosch Data',2,'[{\"changed\": {\"fields\": [\"creation_date\"]}}]',2,1),(55,'2020-03-13 20:10:22.729552','14','Nordland IT Medien',2,'[{\"changed\": {\"fields\": [\"creation_date\"]}}]',2,1),(56,'2020-03-13 20:10:36.417990','15','DATABLOCK EDV-Systeme',2,'[{\"changed\": {\"fields\": [\"creation_date\"]}}]',2,1),(57,'2020-03-13 20:10:47.039156','16','Secret Software',2,'[{\"changed\": {\"fields\": [\"creation_date\"]}}]',2,1),(58,'2020-03-13 20:10:57.745628','17','Siemens AG',2,'[{\"changed\": {\"fields\": [\"creation_date\"]}}]',2,1),(59,'2020-03-13 20:11:07.567731','18','AXA Stephan Lamp',2,'[{\"changed\": {\"fields\": [\"creation_date\"]}}]',2,1),(60,'2020-03-13 20:11:20.583061','19','C319 IT Services',2,'[{\"changed\": {\"fields\": [\"creation_date\"]}}]',2,1),(61,'2020-03-13 20:16:04.852122','1','Hans Müller',1,'[{\"added\": {}}]',3,1),(62,'2020-03-13 20:17:44.889884','2','Max Musterman',1,'[{\"added\": {}}]',3,1),(63,'2020-03-13 20:18:18.326727','3','Marie Mars',1,'[{\"added\": {}}]',3,1),(64,'2020-03-13 20:18:48.450990','4','Sophie Paulsen',1,'[{\"added\": {}}]',3,1),(65,'2020-03-13 20:19:30.751636','5','Martin Hansen',1,'[{\"added\": {}}]',3,1),(66,'2020-03-13 20:20:41.955090','6','Norbert Salzer',1,'[{\"added\": {}}]',3,1),(67,'2020-03-13 20:21:26.139935','7','Anika Braasch',1,'[{\"added\": {}}]',3,1),(68,'2020-03-13 20:21:54.772886','8','Leon Meier',1,'[{\"added\": {}}]',3,1),(69,'2020-03-13 20:22:25.914394','9','Matthias Vossen',1,'[{\"added\": {}}]',3,1),(70,'2020-03-13 20:23:09.011109','10','Lena Schlüter',1,'[{\"added\": {}}]',3,1),(71,'2020-03-13 20:23:45.723161','11','Ralf Münster',1,'[{\"added\": {}}]',3,1),(72,'2020-03-13 20:24:54.379124','12','Finn Petersen',1,'[{\"added\": {}}]',3,1),(73,'2020-03-13 20:25:19.769700','13','Marvin Klaus',1,'[{\"added\": {}}]',3,1),(74,'2020-03-13 20:26:03.864705','14','Lene Hansen',1,'[{\"added\": {}}]',3,1),(75,'2020-03-13 20:26:57.369489','15','Eva Kasiske',1,'[{\"added\": {}}]',3,1),(76,'2020-03-13 20:27:24.658668','16','Paul Paulsen',1,'[{\"added\": {}}]',3,1),(77,'2020-03-13 20:27:55.842684','17','Sina Stark',1,'[{\"added\": {}}]',3,1),(78,'2020-03-13 20:28:42.807093','18','Fred Carstensen',1,'[{\"added\": {}}]',3,1),(79,'2020-03-13 20:29:16.504028','19','Nele Nielsen',1,'[{\"added\": {}}]',3,1),(80,'2020-03-13 20:29:31.391157','10','Lena Schlüter',2,'[{\"changed\": {\"fields\": [\"photo\"]}}]',3,1),(81,'2020-03-15 22:28:41.607415','7','Alle',1,'[{\"added\": {}}]',6,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-15 23:29:33
