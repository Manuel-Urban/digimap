# TechMap

Die TechMap ist eine Seite auf der sich IT-Unternehmen ganz Schleswig-Holsteins kostenlos registrieren können. Die Unternehmen können sich über ihre Unternehmensseite kurz präsentieren und eine Kontaktperson angeben, die für Fragen bereitsteht.

## Kurzer Einblick in die Funktionen
Jede Seite hat den gleichen Header um eine intuitive Bedienung zu gewährleisten. Dieser Navigiert zur Kontaktseite, den Statistiken, der Profilseite, sofern ein Benutzer angemeldet ist und dem Logout oder Login. Im Falle, dass kein Benutzer angemeldet ist enthält der Header ebenfalls eine Navigation zur Registierung.
Der Footer ist ebenfalls immer der selbe und umfasst die Navigation zum Impressum und der Datenschutzgrundverordnung. 

### Startseite 
Die Startseite bietet einen kurzen Überblick über die Funktion der Webseite. Dazu enthält sie eine Freitextsuche mit der man Unternehmen problemlos suchen kann, wenn man den Namen oder nur einen Teil davon kennt. Wenn man sich lieber am Standort des Unternehmens oder dem Bereich orientieren möchte, ist unter der Suche eine Map, auf der alle Unternehmen Schleswig-Holsteins eingeblendet sind. Diese Unternehmen kann man nach Kategorie und/oder Unternehmensform gefiltert anzeigen, um die Such zu erleichtern.
Ebenfalls vorhanden ist eine Übersicht über alle Unternehmen, die bei der TechMap registriert sind.

### Kontakt
Die Kontaktseite enthält ein Formular für damit Benutzer den admin kontaktieren können, wenn beispielsweise insolvent gegangene Unternehmen immernoch zu sehen sind, falsche Unternehmen gefunden wurden oder es einfach nur allgemeine Fragen zur Seite gibt.

### Statistiken
Diese Seite ist eine kleine Info Seite über die TechMap, die ein paar Infos über alle Unternehmen enthält. Zum Beispiel zeigt die sie Anzahl aller Unternehmen pro Kategorie. 

### Profil
Auf dieser Seite findet man alle Unternehmen die jemals unter dem angemeldeten Benutzer erstellt wurden. Dort kann man neue Unternehmen erstellen und alte aktualisieren oder löschen. Ebenfalls legt man dort die Kontaktperson an oder ändert diese bei Bedarf wieder.

## Installation
Eine Anleitung zur Installation befindet sich im Repository ("Anleitung.pdf"). Im Repository befindet sich zudem auch ein Ordner der "Datenbank-Dump" heißt, in dem sich die Testdatenbank befindet, die zu importieren ist. Wie genau dies alles abläuft wird in der Anleitung erklärt.
Wenn alles geklappt hat und der Server läuft, erreicht man die Seite unter <http://localhost:8000/>

## Anmerkungen zur Umsetzung
### Admin-Page
Da es uns wichtig war, dass sich keine falschen Unternehmen bei der TechMap anmelden können, haben wir einen Admin erstellt. Dieser Admin muss die Unternehmen frei geben, bevor sie auf der Seite erscheinen. Damit behalten die Verwalter der TechMap die kontrolle und die TechMap ein seriöses auftreten.
Auf die Adminseite gelangt man unter: <http://localhost:8000/admin/>

### Übersetzung 
Die Übersetzung wurde von uns auf der Indexseite angedeutet. Da wir es aus Zeitgründen nicht mehr geschafft haben, die Sprache auch über die Seite umschaltbar zu machen, muss man dies über die settings.py im code vornehmen. 
![Semantic description of image](/readMeImg/language.png "Language settings")

Übersetze Seite: 
![Semantic description of image](/readMeImg/translation.png "Translated page")

### Nicht Umgesetzt 
Aus Zeitgründen haben wir ein paar Sachen mit in unseren Augen geringer Priorität nicht weiter umgesetzt.
Nicht umgesetzt haben wir:
- Scrolleffekt bei der Karte ausstellen
- Logging Mechanismus (temporär war einer für die adminseite eingebaut, aber dieser war dort in unseren Augen nicht sinnvoll)
- CI/CD Pipeline lauffähig machen --> wir haben eine eigene kleine Pipeline auf unserem privatem gitlab erstellt
- zweite Statistik über Unternehmenszuwachs
- Umschalten der Sprache über die Webseite

### Kleiner Bug beim Import der Datenbank
Leider haben wir den Bug nicht behoben bekommen, dass wenn man die Unternehmen über den Dump importiert, alls mitten im Meer angezeigt werden. Man muss einmal auf das Unternehmen raufgehen und die Adresse "ändern" und neu speichern, damit dies richtig angezeigt wird. 
Wenn das sehr schlimm ist, dann werden wir nochmals nach einer Lösung suchen.

![Semantic description of image](/readMeImg/dots.png "dots on map")